using Application.Commons;
using Infrastructures;
using small_e_commerce;

var builder = WebApplication.CreateBuilder(args);

var config = builder.Configuration.Get<AppConfiguration>();
builder.Services.AddInfrastructuresService(config);
builder.Services.AddServices(config);
builder.Services.AddSingleton(config);

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

app.Run();