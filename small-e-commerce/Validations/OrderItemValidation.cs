﻿using Application.DTO;
using FluentValidation;

namespace small_e_commerce.Validations
{
    public class OrderItemValidation : AbstractValidator<OrderItemDTO>
    {
        public OrderItemValidation()
        {
            RuleFor(x => x.OrderId).NotEmpty();
            RuleFor(x => x.ProductId).NotEmpty();
            RuleFor(x => x.Price).NotEmpty();
            RuleFor(x => x.Quantity).NotEmpty();
        }
    }
}
