﻿using Application.DTO;
using FluentValidation;

namespace small_e_commerce.Validations
{
    public class OrderValidation : AbstractValidator<OrderDTO>
    {
        public OrderValidation()
        {
            RuleFor(x => x.CustomerId).NotEmpty();
            RuleFor(x => x.OrderDate).NotEmpty();
            RuleFor(x => x.Address).NotEmpty();
            RuleFor(x => x.Amount).NotEmpty();
        }
    }
}
