﻿using Application.DTO;
using FluentValidation;

namespace small_e_commerce.Validations
{
    public class CustomerValidation : AbstractValidator<CustomerDTO>
    {
        public CustomerValidation()
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(30);
            RuleFor(x => x.Age).NotEmpty();
            RuleFor(x => x.Address).NotEmpty();
            RuleFor(x => x.Email).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}
