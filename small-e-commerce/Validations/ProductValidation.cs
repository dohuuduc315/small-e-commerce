﻿using Application.DTO;
using FluentValidation;

namespace small_e_commerce.Validations
{
    public class ProductValidation : AbstractValidator<ProductDTO>
    {
        public ProductValidation()
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(50);
            RuleFor(x => x.Price).NotEmpty();
            RuleFor(x => x.ProductDate).NotEmpty();
        }
    }
}
