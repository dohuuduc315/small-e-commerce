﻿using Application.Commons;
using Application.DTO;
using Application.IServices;
using Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace small_e_commerce.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IValidator<CustomerDTO> _validatorCustomer;
        private readonly IValidator<OrderDTO> _validatorOrder;

        public CustomersController(ICustomerService customerService,
                                   IValidator<CustomerDTO> validatorCustomer,
                                   IValidator<OrderDTO> validatorOrder)
        {
            _customerService = customerService;
            _validatorCustomer = validatorCustomer;
            _validatorOrder = validatorOrder;
        }

        [HttpGet]
        public async Task<Pagination<Customer>> GetAllCustomers(int pageNumber = 0, int pageSize = 10) => await _customerService.GetAllCustomerAsync(pageNumber, pageSize);

        [HttpGet("{id}")]
        public async Task<Customer> GetCustomerById(int id) => await _customerService.GetCustomerByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> CreateCustomer(CustomerDTO customerDTO)
        {
            var validation = _validatorCustomer.Validate(customerDTO);
            if (validation.IsValid)
            {
                var result = await _customerService.AddCustomerAsync(customerDTO);
                if (result is null)
                {
                    return BadRequest("Create fail!");
                }
                return Ok("Create success");
            }
            return BadRequest("Create fail, invalid input information!");
        }

        [HttpPost("{customerId}/orderDTO")]
        public async Task<IActionResult> CreateOrderByCustomerId(int customerId, OrderDTO orderDTO)
        {
            var validation = _validatorOrder.Validate(orderDTO);
            if (validation.IsValid)
            {
                var result = await _customerService.AddOrderByCustomerId(customerId, orderDTO);
                if (result is null)
                {
                    return BadRequest("Create fail!");
                }
                return Ok("Create success");
            }
            return BadRequest("Create fail, invalid input information!");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCustomer(int id, CustomerDTO customerDTO)
        {
            var validation = _validatorCustomer.Validate(customerDTO);
            if (validation.IsValid)
            {
                var result = await _customerService.UpdateCustomerAsync(id, customerDTO);
                if (result is null)
                {
                    return BadRequest("Update fail!");
                }
                return Ok("Update success");
            }
            return BadRequest("Update fail, invalid input information!");
        }

        [HttpDelete("{id}")]
        public async Task DeleteCustomer(int id) => await _customerService.DeleteCustomerAsync(id);

        [HttpGet]
        public async Task<Pagination<Customer>> Search(string name, int age, string address, int pageNumber = 0, int pageSize = 10)
        {
            var result = await _customerService.Searching(name, age, address, pageNumber, pageSize);
            return result;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<string> Login(CustomerLoginDTO customerLoginDTO) => await _customerService.LoginAsync(customerLoginDTO);

    }
}
