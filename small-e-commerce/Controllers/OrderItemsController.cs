﻿using Application.Commons;
using Application.DTO;
using Application.IServices;
using Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace small_e_commerce.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class OrderItemsController : ControllerBase
    {
        private readonly IOrderItemService _orderItemService;
        private readonly IValidator<OrderItemDTO> _validatorOrderItem;

        public OrderItemsController(IOrderItemService orderItemService,
                                    IValidator<OrderItemDTO> validatorOrderItem)
        {
            _orderItemService = orderItemService;
            _validatorOrderItem = validatorOrderItem;
        }

        [HttpGet]
        public async Task<Pagination<OrderItems>> GetAllOrderItems(int pageNumber = 0, int pageSize = 10) => await _orderItemService.GetAllOrderItemAsync(pageNumber, pageSize);

        [HttpGet("{id}")]
        public async Task<OrderItems> GetOrderItemById(int id) => await _orderItemService.GetOrderItemsByIdAsync(id);

        [HttpGet("{orderId}")]
        public async Task<Pagination<OrderItems>> GetListOrderItemByOrderId(int orderId, int pageNumber = 0, int pageSize = 10) => await _orderItemService.GetListOrderItemByOrderIdAsync(orderId, pageNumber, pageSize);

        [HttpPost]
        public async Task<IActionResult> CreateOrderItem(OrderItemDTO orderItemDTO)
        {
            var validation = _validatorOrderItem.Validate(orderItemDTO);
            if (validation.IsValid)
            {
                var result = await _orderItemService.AddOrderItemAsync(orderItemDTO);
                if (result is null)
                {
                    return BadRequest("Create fail!");
                }
                return Ok("Create success");
            }
            return BadRequest("Create fail, invalid input information!");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOrderItem(int id, OrderItemDTO orderItemDTO)
        {
            var validation = _validatorOrderItem.Validate(orderItemDTO);
            if (validation.IsValid)
            {
                var result = await _orderItemService.UpdateOrderItemAsync(id, orderItemDTO);
                if (result is null)
                {
                    return BadRequest("Update fail!");
                }
                return Ok("Update success");
            }
            return BadRequest("Update fail, invalid input information!");
        }

        [HttpPut("{orderId}")]
        public async Task<IActionResult> UpdateOrderItemByOrderId(int orderId, OrderItemDTO orderItemDTO)
        {
            var validation = _validatorOrderItem.Validate(orderItemDTO);
            if (validation.IsValid)
            {
                var result = await _orderItemService.UpdateOrderItemByOrderIdAsync(orderId, orderItemDTO);
                if (result is null)
                {
                    return BadRequest("Update fail");
                }
                return Ok("Update success");
            }
            return BadRequest("Update fail, invalid input information!");
        }

        [HttpDelete("{id}")]
        public async Task DeleteOrderItem(int id) => await _orderItemService.DeleteOrderItemAsync(id);

        [HttpDelete("{orderId}")]
        public async Task DeleteOrderItemByOrderId(int orderId) => await _orderItemService.DeleteOrderItemByOrderIdAsync(orderId);
    }
}
