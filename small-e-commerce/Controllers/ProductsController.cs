﻿using Application.Commons;
using Application.DTO;
using Application.IServices;
using Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace small_e_commerce.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IValidator<ProductDTO> _validatorProduct;

        public ProductsController(IProductService productService,
                                  IValidator<ProductDTO> validatorProduct)
        {
            _productService = productService;
            _validatorProduct = validatorProduct;
        }
        [HttpGet]
        public async Task<Pagination<Product>> GetAllProduct(int pageNumber = 0, int pageSize = 10) => await _productService.GetAllProductAsync(pageNumber, pageSize);

        [HttpGet("{id}")]
        public async Task<Product> GetProductById(int id) => await _productService.GetProductByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> CreateProduct(ProductDTO productDTO)
        {
            var validation = _validatorProduct.Validate(productDTO);
            if (validation.IsValid)
            {
                var result = await _productService.AddProductAsync(productDTO);
                if (result is null)
                {
                    return BadRequest("Create fail!");
                }
                return Ok("Create success");
            }
            return BadRequest("Create fail, invalid input information!");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProduct(int id, ProductDTO productDTO)
        {
            var validation = _validatorProduct.Validate(productDTO);
            if (validation.IsValid)
            {
                var result = await _productService.UpdateProductAsync(id, productDTO);
                if (result is null)
                {
                    return BadRequest("Update fail!");
                }
                return Ok("Update success");
            }
            return BadRequest("Update fail, invalid input information!");
        }

        [HttpDelete("{id}")]
        public async Task DeleteProduct(int id) => await _productService.DeleteProductAsync(id);

        [HttpGet]
        public async Task<Pagination<Product>> Search(string name, decimal price, int pageNumber = 0, int pageSize = 10)
        {
            var result = await _productService.Searching(name, price, pageNumber, pageSize);
            return result;
        }
    }
}
