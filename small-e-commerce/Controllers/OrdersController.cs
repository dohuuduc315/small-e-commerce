﻿using Application.Commons;
using Application.DTO;
using Application.IServices;
using Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace small_e_commerce.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IValidator<OrderDTO> _validatorOrder;
        private readonly IValidator<OrderItemDTO> _validatorOrderItem;

        public OrdersController(IOrderService orderService,
                                IValidator<OrderDTO> validatorOrder,
                                IValidator<OrderItemDTO> validatorOrderItem)
        {
            _orderService = orderService;
            _validatorOrder = validatorOrder;
            _validatorOrderItem = validatorOrderItem;
        }

        [HttpGet]
        public async Task<Pagination<Orders>> GetAllOrders(int pageNumber = 0, int pageSize = 10) => await _orderService.GetAllOrdersAsync(pageNumber, pageSize);

        [HttpGet("{id}")]
        public async Task<Orders> GetOrderById(int id) => await _orderService.GetOrderByIdAsync(id);

        [HttpGet("{customerId}")]
        public async Task<Pagination<Orders>> GetOrdersByCustomerId(int customerId, int pageNumber = 0, int pageSize = 10) => await _orderService.GetOrdersByCustomerId(customerId, pageNumber, pageSize);

        [HttpPost]
        public async Task<IActionResult> CreateOrder(OrderDTO orderDTO)
        {
            var validation = _validatorOrder.Validate(orderDTO);
            if (validation.IsValid)
            {
                var result = await _orderService.AddOrderAsync(orderDTO);
                if (result is null)
                {
                    return BadRequest("Create fail!");
                }
                return Ok("Create success");
            }
            return BadRequest("Create fail, invalid input information!");
        }

        [HttpPost("{orderId}/orderItemDTO")]
        public async Task<IActionResult> CreateOrderItemByOrderId(int orderId, OrderItemDTO orderItemDTO)
        {
            var validation = _validatorOrderItem.Validate(orderItemDTO);
            if (validation.IsValid)
            {
                var result = await _orderService.AddOrderItemByOrderId(orderId, orderItemDTO);
                if (result is null)
                {
                    return BadRequest("Create fail!");
                }
                return Ok("Create success");
            }
            return BadRequest("Create fail, invalid input information!");
        }

        [HttpPut("{id}")]
        public async Task UpdateOrder(int id, OrderDTO orderDTO) => await _orderService.UpdateOrderAsync(id, orderDTO);

        [HttpPut("{customerId}")]
        public async Task<IActionResult> UpdateOrderByCustomerId(int customerId, OrderDTO orderDTO)
        {
            var validation = _validatorOrder.Validate(orderDTO);
            if (validation.IsValid)
            {
                var result = await _orderService.UpdateOrderByCustomerIdAsync(customerId, orderDTO);
                if (result is null)
                {
                    return BadRequest("Update fail!");
                }
                return Ok("Update success");
            }
            return BadRequest("Update fail, invalid input information!");
        }

        [HttpDelete("{id}")]
        public async Task DeleteOrder(int id) => await _orderService.DeleteOrderAsync(id);

        [HttpDelete("{customerId}")]
        public async Task DeleteOrderByCustomerId(int customerId) => await _orderService.DeleteOrderByCustomerIdAsync(customerId);

        [HttpGet]
        public async Task<Pagination<Orders>> Search(string address, decimal amount, int pageNumber = 0, int pageSize = 10)
        {
            var result = await _orderService.Searching(address, amount, pageNumber, pageSize);
            return result;
        }
    }
}
