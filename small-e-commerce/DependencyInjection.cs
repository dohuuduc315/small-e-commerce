﻿using Application.Commons;
using Application.DTO;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using small_e_commerce.Validations;
using System.Text;

namespace small_e_commerce
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddServices(this IServiceCollection services, AppConfiguration appConfiguration)
        {
            services.AddControllers();
            services.AddHttpContextAccessor();

            //Add validation
            services.AddScoped<IValidator<CustomerDTO>, CustomerValidation>();
            services.AddScoped<IValidator<OrderDTO>, OrderValidation>();
            services.AddScoped<IValidator<OrderItemDTO>, OrderItemValidation>();
            services.AddScoped<IValidator<ProductDTO>, ProductValidation>();

            services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(option =>
            {
                option.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appConfiguration.JWTSecretKey))
                };
            });

            return services;
        }
    }
}
