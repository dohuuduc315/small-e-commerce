﻿using Application.Commons;
using Application.DTO;
using Application.IServices;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class OrderItemService : IOrderItemService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderItemService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<OrderItemDTO> AddOrderItemAsync(OrderItemDTO orderItemDTO)
        {
            var orderItemObj = _mapper.Map<OrderItems>(orderItemDTO);
            await _unitOfWork.orderItemRepository.AddAsync(orderItemObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return _mapper.Map<OrderItemDTO>(orderItemObj);
            }
            return null;
        }

        public async Task DeleteOrderItemAsync(int id)
        {
            var orderItemObj = await _unitOfWork.orderItemRepository.GetByIdAsync(id);
            if (orderItemObj is not null)
            {
                _unitOfWork.orderItemRepository.Update(orderItemObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task DeleteOrderItemByOrderIdAsync(int orderId)
        {
            var orderItemObj = await _unitOfWork.orderItemRepository.GetOrderItemByOrderIdAsync(orderId);
            if (orderItemObj is not null)
            {
                _unitOfWork.orderItemRepository.SoftRemove(orderItemObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task<Pagination<OrderItems>> GetAllOrderItemAsync(int pageNumber = 0, int pageSize = 10) => await _unitOfWork.orderItemRepository.ToPagination(pageNumber, pageSize);

        public async Task<Pagination<OrderItems>> GetListOrderItemByOrderIdAsync(int orderId, int pageNumber = 0, int pageSize = 10)
        {
            var orderItemObj = await _unitOfWork.orderItemRepository.GetListOrderItemByOrderIdAsync(orderId, pageNumber, pageSize);
            return orderItemObj;
        }

        public async Task<OrderItems> GetOrderItemsByIdAsync(int id) => await _unitOfWork.orderItemRepository.GetByIdAsync(id);

        public async Task<OrderItemDTO> UpdateOrderItemAsync(int id, OrderItemDTO orderItemDTO)
        {
            var orderItemObj = await _unitOfWork.orderItemRepository.GetByIdAsync(id);
            if (orderItemObj is not null)
            {
                _mapper.Map(orderItemDTO, orderItemObj);
                _unitOfWork.orderItemRepository.Update(orderItemObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<OrderItemDTO>(orderItemObj);
                }
            }
            return null;
        }

        public async Task<OrderItemDTO> UpdateOrderItemByOrderIdAsync(int orderId, OrderItemDTO orderItemDTO)
        {
            var orderItemObj = await _unitOfWork.orderItemRepository.GetOrderItemByOrderIdAsync(orderId);
            if (orderItemObj is not null)
            {
                _mapper.Map(orderItemDTO, orderItemObj);
                _unitOfWork.orderItemRepository.Update(orderItemObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<OrderItemDTO>(orderItemObj);
                }
            }
            return null;
        }
    }
}
