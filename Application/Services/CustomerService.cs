﻿using Application.Commons;
using Application.DTO;
using Application.IServices;
using Application.Utils;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly AppConfiguration _appConfiguration;
        private readonly ICurrentTimeService _currentTimeService;

        public CustomerService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTimeService currentTimeService, AppConfiguration appConfiguration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _appConfiguration = appConfiguration;
            _currentTimeService = currentTimeService;
        }
        public async Task<CustomerDTO> AddCustomerAsync(CustomerDTO customerDTO)
        {
            var isExisted = await _unitOfWork.customerRepository.CheckEmailExisted(customerDTO.Email);

            if (isExisted)
            {
                throw new Exception("Username exited please try again");
            }

            var customerObj = _mapper.Map<Customer>(customerDTO);
            await _unitOfWork.customerRepository.AddAsync(customerObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return _mapper.Map<CustomerDTO>(customerObj);
            }
            return null;
        }

        public async Task<OrderDTO> AddOrderByCustomerId(int customerId, OrderDTO orderDTO)
        {
            var customerObj = await _unitOfWork.customerRepository.GetByIdAsync(customerId);
            if (customerObj is not null)
            {
                var orderObj = _mapper.Map<Orders>(orderDTO);
                await _unitOfWork.orderRepository.AddAsync(orderObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<OrderDTO>(orderObj);
                }
            }
            return null;
        }

        public async Task DeleteCustomerAsync(int id)
        {
            var customerObj = await _unitOfWork.customerRepository.GetByIdAsync(id);
            if (customerObj is not null)
            {
                _unitOfWork.customerRepository.SoftRemove(customerObj);
                await _unitOfWork.SaveChangeAsync();
            }
            
        }

        public async Task<Pagination<Customer>> GetAllCustomerAsync(int pageNumber = 0, int pageSize = 10) => await _unitOfWork.customerRepository.ToPagination(pageNumber, pageSize);

        public async Task<Customer> GetCustomerByIdAsync(int id) => await _unitOfWork.customerRepository.GetByIdAsync(id);

        public async Task<string> LoginAsync(CustomerLoginDTO customerLoginDTO)
        {
            var customer = await _unitOfWork.customerRepository.GetCustomerByEmailAndPassword(customerLoginDTO.Email, customerLoginDTO.Password);
            var token = customer.GenerateJsonWebToken(_appConfiguration.JWTSecretKey, _currentTimeService.GetCurrentTime());
            return token;
        }

        public async Task<Pagination<Customer>> Searching(string name, int age, string address, int pageNumber = 0, int pageSize = 10)
        {
            var customerObj = await _unitOfWork.customerRepository.Searching(name, age, address, pageNumber, pageSize);
            return customerObj;
        }

        public async Task<CustomerDTO> UpdateCustomerAsync(int id, CustomerDTO customerDTO)
        {
            var customerObj = await _unitOfWork.customerRepository.GetByIdAsync(id);
            if (customerObj is not null)
            {
                _mapper.Map(customerDTO, customerObj);
                _unitOfWork.customerRepository.Update(customerObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<CustomerDTO>(customerObj);
                }
            }
            return null;
        }
    }
}
