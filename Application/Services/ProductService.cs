﻿using Application.Commons;
using Application.DTO;
using Application.IServices;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<ProductDTO> AddProductAsync(ProductDTO productDTO)
        {
            var productObj = _mapper.Map<Product>(productDTO);
            await _unitOfWork.productRepository.AddAsync(productObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return _mapper.Map<ProductDTO>(productObj);
            }
            return null;
        }

        public async Task DeleteProductAsync(int id)
        {
            var productObj = await _unitOfWork.productRepository.GetByIdAsync(id);
            if (productObj is not null)
            {
                _unitOfWork.productRepository.SoftRemove(productObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task<Pagination<Product>> GetAllProductAsync(int pageNumber = 0, int pageSize = 10) => await _unitOfWork.productRepository.ToPagination(pageNumber, pageSize);

        public async Task<Product> GetProductByIdAsync(int id) => await _unitOfWork.productRepository.GetByIdAsync(id);

        public async Task<Pagination<Product>> Searching(string name, decimal price, int pageNumber = 0, int pageSize = 10)
        {
            var productObj = await _unitOfWork.productRepository.Searching(name, price, pageNumber, pageSize);
            return productObj;
        }

        public async Task<ProductDTO> UpdateProductAsync(int id, ProductDTO productDTO)
        {
            var productObj = await _unitOfWork.productRepository.GetByIdAsync(id);
            if (productObj is not null)
            {
                _mapper.Map(productDTO, productObj);
                _unitOfWork.productRepository.Update(productObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<ProductDTO>(productObj);
                }
            }
            return null;
        }
    }
}
