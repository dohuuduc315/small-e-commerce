﻿using Application.IServices;

namespace Application.Services
{
    public class CurrentTimeService : ICurrentTimeService
    {
        public DateTime GetCurrentTime() => DateTime.UtcNow;
    }
}
