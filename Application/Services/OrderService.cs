﻿using Application.Commons;
using Application.DTO;
using Application.IServices;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<OrderDTO> AddOrderAsync(OrderDTO orderDTO)
        {
            var orderObj = _mapper.Map<Orders>(orderDTO);
            await _unitOfWork.orderRepository.AddAsync(orderObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return _mapper.Map<OrderDTO>(orderObj);
            }
            return null;
        }

        public async Task<OrderItemDTO> AddOrderItemByOrderId(int orderId, OrderItemDTO orderItemDTO)
        {
            var orderObj = await _unitOfWork.orderRepository.GetByIdAsync(orderId);
            if (orderObj is not null)
            {
                var orderItemObj = _mapper.Map<OrderItems>(orderItemDTO);
                await _unitOfWork.orderItemRepository.AddAsync(orderItemObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<OrderItemDTO>(orderItemObj);
                }
            }
            return null;
        }

        public async Task DeleteOrderAsync(int id)
        {
            var orderObj = await _unitOfWork.orderRepository.GetByIdAsync(id);
            if (orderObj is not null)
            {
                _unitOfWork.orderRepository.SoftRemove(orderObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task DeleteOrderByCustomerIdAsync(int customerId)
        {
            var orderObj = await _unitOfWork.orderRepository.GetOrderByCustomerIdAsync(customerId);
            if (orderObj is not null)
            {
                _unitOfWork.orderRepository.SoftRemove(orderObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task<Pagination<Orders>> GetAllOrdersAsync(int pageNumber = 0, int pageSize = 10) => await _unitOfWork.orderRepository.ToPagination(pageNumber, pageSize);

        public async Task<Orders> GetOrderByIdAsync(int id) => await _unitOfWork.orderRepository.GetByIdAsync(id);

        public async Task<Pagination<Orders>> GetOrdersByCustomerId(int customerId, int pageNumber = 0, int pageSize = 10)
        {
            var orderObj = await _unitOfWork.orderRepository.GetOrdersByCustomerIdAsync(customerId, pageNumber, pageSize);
            return orderObj;
        }

        public async Task<Pagination<Orders>> Searching(string address, decimal amount, int pageNumber = 0, int pageSize = 10)
        {
            var orderObj = await _unitOfWork.orderRepository.Searching(address, amount, pageNumber, pageSize);
            return orderObj;
        }

        public async Task<OrderDTO> UpdateOrderAsync(int id, OrderDTO orderDTO)
        {
            var orderObj = await _unitOfWork.orderRepository.GetByIdAsync(id);
            if (orderObj is not null)
            {
                _mapper.Map(orderDTO, orderObj);
                _unitOfWork.orderRepository.Update(orderObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<OrderDTO>(orderObj);
                }
            }
            return null;
        }

        public async Task<OrderDTO> UpdateOrderByCustomerIdAsync(int customerId, OrderDTO orderDTO)
        {
            var orderObj = await _unitOfWork.orderRepository.GetOrderByCustomerIdAsync(customerId);
            if (orderObj is not null)
            {
                _mapper.Map(orderDTO, orderObj);
                _unitOfWork.orderRepository.Update(orderObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<OrderDTO>(orderObj);
                }
            }
            return null;
        }
    }
}
