﻿using Application.IServices;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace Application.Services
{
    public class ClaimsService : IClaimsService
    {
        public ClaimsService(IHttpContextAccessor httpContextAccessor)
        {
            var id = httpContextAccessor.HttpContext?.User?.FindFirstValue("UserId");
            CurrentUserId = string.IsNullOrEmpty(id) ? 0 : int.Parse(id);
        }
        public int CurrentUserId { get; }
    }
}
