﻿namespace Application.IServices
{
    public interface IClaimsService
    {
        public int CurrentUserId { get; }
    }
}
