﻿using Application.Commons;
using Application.DTO;
using Domain.Entities;

namespace Application.IServices
{
    public interface IProductService
    {
        Task<ProductDTO> AddProductAsync(ProductDTO productDTO);
        Task<ProductDTO> UpdateProductAsync(int id, ProductDTO productDTO);
        Task DeleteProductAsync(int id);
        Task<Product> GetProductByIdAsync(int id);
        Task<Pagination<Product>> GetAllProductAsync(int pageNumber = 0, int pageSize = 10);
        Task<Pagination<Product>> Searching(string name, decimal price, int pageNumber = 0, int pageSize = 10);
    }
}
