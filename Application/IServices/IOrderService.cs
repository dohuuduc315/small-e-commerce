﻿using Application.Commons;
using Application.DTO;
using Domain.Entities;

namespace Application.IServices
{
    public interface IOrderService
    {
        Task<OrderDTO> AddOrderAsync(OrderDTO orderDTO);
        Task<OrderDTO> UpdateOrderAsync(int id, OrderDTO orderDTO);
        Task DeleteOrderAsync(int id);
        Task<Orders> GetOrderByIdAsync(int id);
        Task<Pagination<Orders>> GetAllOrdersAsync(int pageNumber = 0, int pageSize = 10);
        Task<Pagination<Orders>> GetOrdersByCustomerId(int customerId, int pageNumber = 0, int pageSize = 10);
        Task<OrderDTO> UpdateOrderByCustomerIdAsync(int customerId, OrderDTO orderDTO);
        Task DeleteOrderByCustomerIdAsync(int customerId);
        Task<OrderItemDTO> AddOrderItemByOrderId(int orderId, OrderItemDTO orderItemDTO);
        Task<Pagination<Orders>> Searching(string address, decimal amount, int pageNumber = 0, int pageSize = 10);
    }
}
