﻿namespace Application.IServices
{
    public interface ICurrentTimeService
    {
        public DateTime GetCurrentTime();
    }
}
