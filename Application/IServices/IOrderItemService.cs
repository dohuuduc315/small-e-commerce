﻿using Application.Commons;
using Application.DTO;
using Domain.Entities;

namespace Application.IServices
{
    public interface IOrderItemService
    {
        Task<OrderItemDTO> AddOrderItemAsync(OrderItemDTO orderItemDTO);
        Task<OrderItemDTO> UpdateOrderItemAsync(int id, OrderItemDTO orderItemDTO);
        Task DeleteOrderItemAsync(int id);
        Task<OrderItems> GetOrderItemsByIdAsync(int id);
        Task<Pagination<OrderItems>> GetAllOrderItemAsync(int pageNumber = 0, int pageSize = 10);
        Task<Pagination<OrderItems>> GetListOrderItemByOrderIdAsync(int orderId, int pageNumber = 0, int pageSize = 10);
        Task<OrderItemDTO> UpdateOrderItemByOrderIdAsync(int orderId, OrderItemDTO orderItemDTO);
        Task DeleteOrderItemByOrderIdAsync(int orderId);
    }
}
