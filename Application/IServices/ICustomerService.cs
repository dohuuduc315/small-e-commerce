﻿using Application.Commons;
using Application.DTO;
using Domain.Entities;

namespace Application.IServices
{
    public interface ICustomerService
    {
        Task<CustomerDTO> AddCustomerAsync(CustomerDTO customerDTO);
        Task<CustomerDTO> UpdateCustomerAsync(int id, CustomerDTO customerDTO);
        Task DeleteCustomerAsync(int id);
        Task<Customer> GetCustomerByIdAsync(int id);
        Task<Pagination<Customer>> GetAllCustomerAsync(int pageNumber = 0, int pageSize = 10);
        Task<OrderDTO> AddOrderByCustomerId(int customerId, OrderDTO orderDTO);
        Task<Pagination<Customer>> Searching(string name, int age, string address, int pageNumber = 0, int pageSize = 10);
        public Task<string> LoginAsync(CustomerLoginDTO customerLoginDTO);
    }
}
