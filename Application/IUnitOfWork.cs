﻿using small_e_commerce.IRepositories;

namespace Application
{
    public interface IUnitOfWork : IDisposable
    {
        public ICustomerRepository customerRepository { get; }
        public IProductRepository productRepository { get; }
        public IOrderItemRepository orderItemRepository { get; }
        public IOrderRepository orderRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
