﻿namespace Application.DTO
{
    public class CustomerLoginDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
