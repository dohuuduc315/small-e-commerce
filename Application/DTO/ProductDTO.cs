﻿namespace Application.DTO
{
    public class ProductDTO
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public DateTime ProductDate { get; set; }
        public string Description { get; set; }
    }
}
