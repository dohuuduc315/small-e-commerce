﻿namespace Application.DTO
{
    public class OrderDTO
    {
        public int CustomerId { get; set; }
        public DateTime OrderDate { get; set; }
        public string Address { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
    }
}
