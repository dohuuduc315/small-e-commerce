﻿using Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Application.Utils
{
    public static class GenerateJsonWebTokenString
    {
        public static string GenerateJsonWebToken(this Customer customer, string secretkey, DateTime now)
        {
            var secretInBytes = Encoding.UTF8.GetBytes(secretkey);
            var securityKey = new SymmetricSecurityKey(secretInBytes);
            var credentials = new SigningCredentials(securityKey, "HS256");

            var claims = new List<Claim>()
            {
                new Claim("UserId", customer.Id.ToString()),
                new Claim(ClaimTypes.Role, customer.Role.ToString())
            };

            var token = new JwtSecurityToken(
                claims: claims,
                expires: now.AddMinutes(15),
                signingCredentials: credentials
            );

            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = jwtTokenHandler.WriteToken(token);

            return jwtToken;
        }
    }
}
