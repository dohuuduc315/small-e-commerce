﻿using Application.Commons;
using Domain.Entities;

namespace small_e_commerce.IRepositories
{
    public interface IOrderItemRepository : IGenericRepository<OrderItems>
    {
        Task<Pagination<OrderItems>> GetListOrderItemByOrderIdAsync(int orderId, int pageNumber = 0, int pageSize = 10);
        Task<OrderItems> GetOrderItemByOrderIdAsync(int orderId);
    }
}
