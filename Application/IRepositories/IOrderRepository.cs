﻿using Application.Commons;
using Domain.Entities;

namespace small_e_commerce.IRepositories
{
    public interface IOrderRepository : IGenericRepository<Orders>
    {
        Task<Pagination<Orders>> GetOrdersByCustomerIdAsync(int customerId, int pageNumber = 0, int pageSize = 10);
        Task<Orders> GetOrderByCustomerIdAsync(int customerId);
        Task<Pagination<Orders>> Searching(string address, decimal amount, int pageNumber = 0, int pageSize = 10);
    }
}
