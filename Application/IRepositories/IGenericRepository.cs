﻿using Application.Commons;
using Domain.Entities;

namespace small_e_commerce.IRepositories
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task AddAsync(T entity);
        void Update(T entity);
        void SoftRemove(T entity);
        Task<List<T>> GetAllAsync();
        Task<T?> GetByIdAsync(int id);
        Task<Pagination<T>> ToPagination(int pageNumber = 0, int pageSize = 10);
    }
}
