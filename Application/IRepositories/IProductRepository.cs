﻿using Application.Commons;
using Domain.Entities;

namespace small_e_commerce.IRepositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<Pagination<Product>> Searching(string name, decimal price, int pageNumber = 0, int pageSize = 10);
    }
}
