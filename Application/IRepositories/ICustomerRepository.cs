﻿using Application.Commons;
using Domain.Entities;

namespace small_e_commerce.IRepositories
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        Task<Pagination<Customer>> Searching(string name, int age, string address, int pageNumber = 0, int pageSize = 10);
        Task<Customer> GetCustomerByEmailAndPassword(string email, string password);
        Task<bool> CheckEmailExisted(string email);
    }
}
