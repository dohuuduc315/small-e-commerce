﻿namespace Domain.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public DateTime ProductDate { get; set; }
        public string Description { get; set; }
        public OrderItems OrderItems { get; set; }
    }
}
