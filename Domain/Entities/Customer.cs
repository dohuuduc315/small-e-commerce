﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Customer : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public Role Role { get; set; }
        public string? Token { get; set; }
        public ICollection<Orders> Orders { get; set; }
    }
}
