﻿namespace Domain.Entities
{
    public class Orders : BaseEntity
    {
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public DateTime OrderDate { get; set; }
        public string Address { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public ICollection<OrderItems> OrderItems { get; set; }
    }
}
