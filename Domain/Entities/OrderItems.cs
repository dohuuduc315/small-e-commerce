﻿namespace Domain.Entities
{
    public class OrderItems : BaseEntity
    {
        public int OrderId { get; set; }
        public Orders Order { get; set; }
        public Product Product { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}
