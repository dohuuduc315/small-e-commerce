﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class OrderItemConfig : IEntityTypeConfiguration<OrderItems>
    {
        public void Configure(EntityTypeBuilder<OrderItems> builder)
        {
            builder.HasKey(orderItem => orderItem.Id);
            
            builder.HasOne(orderItem => orderItem.Order)
                   .WithMany(order => order.OrderItems)
                   .HasForeignKey(orderItem => orderItem.OrderId);

            builder.HasOne(orderItem => orderItem.Product)
                   .WithOne(product => product.OrderItems)
                   .HasForeignKey<OrderItems>(orderItem => orderItem.ProductId);
        }
    }
}
