﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        public DbSet<Customer> customers { get; set; }
        public DbSet<OrderItems> orderItems { get; set; }
        public DbSet<Product> products { get; set; }
        public DbSet<Orders> orders { get; set; }
    }
}
