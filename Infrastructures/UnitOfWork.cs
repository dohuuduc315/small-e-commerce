﻿using Application;
using small_e_commerce.IRepositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IProductRepository _productRepository;

        public UnitOfWork(AppDbContext context,
                          ICustomerRepository customerRepository,
                          IOrderItemRepository orderItemRepository,
                          IOrderRepository orderRepository,
                          IProductRepository productRepository)
        {
            _context = context;
            _customerRepository = customerRepository;
            _orderItemRepository = orderItemRepository;
            _orderRepository = orderRepository;
            _productRepository = productRepository;
        }
        public ICustomerRepository customerRepository => _customerRepository;

        public IProductRepository productRepository => _productRepository;

        public IOrderItemRepository orderItemRepository => _orderItemRepository;

        public IOrderRepository orderRepository => _orderRepository;

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<int> SaveChangeAsync() => await _context.SaveChangesAsync();
    }
}
