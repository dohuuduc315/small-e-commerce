﻿using Microsoft.Extensions.DependencyInjection;
using small_e_commerce.IRepositories;
using Microsoft.EntityFrameworkCore;
using Application.IServices;
using Application;
using Application.Services;
using Infrastructures.Repositories;
using Application.Commons;

namespace Infrastructures
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructuresService(this IServiceCollection services, AppConfiguration appConfiguration)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IOrderItemService, OrderItemService>();
            services.AddScoped<IOrderItemRepository, OrderItemRepository>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICurrentTimeService, CurrentTimeService>();
            services.AddScoped<IClaimsService, ClaimsService>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddDbContext<AppDbContext>(option => option.UseSqlServer(appConfiguration.DatabaseConnection));

            return services;
        }
    }
}
