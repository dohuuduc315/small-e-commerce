﻿using Application.Commons;
using Application.IServices;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using small_e_commerce.IRepositories;

namespace Infrastructures.Repositories
{
    public class OrderItemRepository : GenericRepository<OrderItems>, IOrderItemRepository
    {
        private readonly AppDbContext _context;

        public OrderItemRepository(AppDbContext context,
                                   ICurrentTimeService currentTimeService,
                                   IClaimsService claimsService)
                                   : base(context,
                                          currentTimeService,
                                          claimsService)
        {
            _context = context;
        }

        public async Task<Pagination<OrderItems>> GetListOrderItemByOrderIdAsync(int orderId, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _context.orderItems.CountAsync();

            var orderItemObj = await _dbSet.Where(x => x.OrderId.Equals(orderId))
                                           .OrderBy(x => x.OrderId)
                                           .Skip(pageNumber * pageSize)
                                           .Take(pageSize)
                                           .AsNoTracking()
                                           .ToListAsync();

            var pagination = new Pagination<OrderItems>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = orderItemObj
            };

            return pagination;
        }

        public async Task<OrderItems> GetOrderItemByOrderIdAsync(int orderId)
        {
            var orderObj = await _dbSet.Where(x => x.OrderId.Equals(orderId))
                                       .OrderBy(x => x.OrderId)
                                       .FirstOrDefaultAsync();
            return orderObj;
        }
    }
}
