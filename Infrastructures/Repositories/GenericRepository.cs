﻿using Application.Commons;
using Application.IServices;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using small_e_commerce.IRepositories;

namespace Infrastructures.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly AppDbContext _context;
        protected DbSet<T> _dbSet;
        private readonly ICurrentTimeService _currentTimeService;
        private readonly IClaimsService _claimsService;

        public GenericRepository(AppDbContext context, ICurrentTimeService currentTimeService, IClaimsService claimsService)
        {
            _context = context;
            _dbSet = _context.Set<T>();
            _currentTimeService = currentTimeService;
            _claimsService = claimsService;
        }
        public async Task AddAsync(T entity) => await _dbSet.AddAsync(entity);

        public Task<List<T>> GetAllAsync() => _dbSet.ToListAsync();

        public async Task<T?> GetByIdAsync(int id) => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public void SoftRemove(T entity)
        {
            entity.isDeleted = true;
            _dbSet.Update(entity);
        }

        public async Task<Pagination<T>> ToPagination(int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();
            var pagination = new Pagination<T>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items
            };
            return pagination;
        }

        public void Update(T entity) => _dbSet.Update(entity);
    }
}
