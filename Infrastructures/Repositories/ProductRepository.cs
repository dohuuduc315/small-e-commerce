﻿using Application.Commons;
using Application.IServices;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using small_e_commerce.IRepositories;

namespace Infrastructures.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly AppDbContext _context;

        public ProductRepository(AppDbContext context, 
                                 ICurrentTimeService currentTimeService,
                                 IClaimsService claimsService)
                                 : base(context,
                                        currentTimeService,
                                        claimsService)
        {
            _context = context;
        }

        public async Task<Pagination<Product>> Searching(string name, decimal price, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _context.products.CountAsync();

            var productObj = await _dbSet.OrderBy(x => x.Name)
                                         .Skip(pageNumber * pageSize)
                                         .Take(pageSize)
                                         .AsNoTracking()
                                         .ToListAsync();

            if (!string.IsNullOrEmpty(name))
            {
                productObj = productObj.Where(x => x.Name.Contains(name)).ToList();
            }

            else if (price != null)
            {
                productObj = productObj.Where(x => x.Price.Equals(price)).ToList();
            }

            var pagination = new Pagination<Product>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = productObj
            };

            return pagination;
        }
    }
}
