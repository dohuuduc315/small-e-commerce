﻿using Application.Commons;
using Application.IServices;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using small_e_commerce.IRepositories;

namespace Infrastructures.Repositories
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {

        private readonly AppDbContext _context;

        public CustomerRepository(AppDbContext context,
                                  ICurrentTimeService currentTimeService,
                                  IClaimsService claimsService)
                                  : base(context,
                                         currentTimeService,
                                         claimsService)
        {
            _context = context;
        }

        public async Task<bool> CheckEmailExisted(string email) => await _context.customers.AnyAsync(x => x.Email == email);

        public async Task<Customer> GetCustomerByEmailAndPassword(string email, string password)
        {
            var customer = await _context.customers.FirstOrDefaultAsync(x => x.Email == email && x.Password == password);
            
            if (customer is null)
            {
                throw new Exception("Invalid Email or password");
            }

            return customer;
        }

        public async Task<Pagination<Customer>> Searching(string name, int age, string address, int pageNumber = 0, int pageSize = 10)
        {
            var customerObj = _dbSet.AsQueryable();

            if (!string.IsNullOrEmpty(name))
            {
                customerObj = _dbSet.Where(x => x.Name.Contains(name));
            }

            else if (age != null)
            {
                customerObj = _dbSet.Where(x => x.Age.Equals(age));
            }

            else if (!string.IsNullOrEmpty(address))
            {
                customerObj = _dbSet.Where(x => x.Address.Contains(address));
            }

            var itemCount = await customerObj.CountAsync();

            var items = await customerObj.ToListAsync();

            var pagination = new Pagination<Customer>
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items
            };
            return pagination;
        }
    }
}
