﻿using Application.Commons;
using Application.IServices;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using small_e_commerce.IRepositories;

namespace Infrastructures.Repositories
{
    public class OrderRepository : GenericRepository<Orders>, IOrderRepository
    {
        private readonly AppDbContext _context;

        public OrderRepository(AppDbContext context,
                               ICurrentTimeService currentTimeService,
                               IClaimsService claimsService)
                               : base(context,
                                      currentTimeService,
                                      claimsService)
        {
            _context = context;
        }

        public async Task<Orders> GetOrderByCustomerIdAsync(int customerId)
        {
            var order = await _dbSet.Where(x => x.CustomerId
                                    .Equals(customerId))
                                    .OrderBy(x => x.CustomerId)
                                    .FirstOrDefaultAsync();
            return order;
        }

        public async Task<Pagination<Orders>> GetOrdersByCustomerIdAsync(int customerId, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _context.orders.CountAsync();
            var orders = await _dbSet.Where(x => x.CustomerId.Equals(customerId))
                                     .OrderBy(x => x.CustomerId)
                                     .Skip(pageNumber * pageSize)
                                     .Take(pageSize)
                                     .AsNoTracking()
                                     .ToListAsync();
            var pagination = new Pagination<Orders>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = orders
            };
            return pagination;
        }

        public async Task<Pagination<Orders>> Searching(string address, decimal amount, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _context.orders.CountAsync();

            var orderObj = await _dbSet.OrderBy(x => x.CustomerId)
                                 .Skip(pageNumber * pageNumber)
                                 .Take(pageSize)
                                 .AsNoTracking()
                                 .ToListAsync();

            if (!string.IsNullOrEmpty(address))
            {
                orderObj = orderObj.Where(x => x.Address.Contains(address)).ToList();
            }

            else if (amount != null)
            {
                orderObj = orderObj.Where(x => x.Amount.Equals(amount)).ToList();
            }

            var pagination = new Pagination<Orders>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = orderObj
            };
            return pagination;
        }
    }
}
