﻿using Application.DTO;
using AutoMapper;
using Domain.Entities;

namespace Infrastructures.Mappers
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<CustomerDTO, Customer>().ReverseMap();
            CreateMap<OrderItemDTO, OrderItems>().ReverseMap();
            CreateMap<OrderDTO, Orders>().ReverseMap();
            CreateMap<ProductDTO, Product>().ReverseMap();
            CreateMap<CustomerLoginDTO, Customer>().ReverseMap();
        }
    }
}
